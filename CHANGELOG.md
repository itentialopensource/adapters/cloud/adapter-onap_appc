
## 0.6.4 [10-15-2024]

* Changes made at 2024.10.14_21:20PM

See merge request itentialopensource/adapters/adapter-onap_appc!19

---

## 0.6.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-onap_appc!17

---

## 0.6.2 [08-14-2024]

* Changes made at 2024.08.14_19:37PM

See merge request itentialopensource/adapters/adapter-onap_appc!16

---

## 0.6.1 [08-07-2024]

* Changes made at 2024.08.06_21:41PM

See merge request itentialopensource/adapters/adapter-onap_appc!15

---

## 0.6.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!14

---

## 0.5.6 [03-28-2024]

* Changes made at 2024.03.28_13:26PM

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!13

---

## 0.5.5 [03-21-2024]

* Changes made at 2024.03.21_14:05PM

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!12

---

## 0.5.4 [03-11-2024]

* Changes made at 2024.03.11_15:38PM

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!11

---

## 0.5.3 [02-28-2024]

* Changes made at 2024.02.28_11:53AM

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!10

---

## 0.5.2 [12-26-2023]

* update axios and metadata

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!9

---

## 0.5.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!8

---

## 0.5.0 [12-14-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!7

---

## 0.4.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!6

---

## 0.3.3 [03-11-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!5

---

## 0.3.2 [07-08-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!4

---

## 0.3.1 [01-13-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!3

---

## 0.3.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!2

---

## 0.2.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-onap_appc!1

---

## 0.1.1 [08-26-2019]

- Initial Commit

See commit 0d12784

---
