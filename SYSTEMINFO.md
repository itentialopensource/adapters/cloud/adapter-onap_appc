# ONAP Application Controller

Vendor: ONAP
Homepage: https://www.onap.org/

Product: ONAP Application Controller
Product Page: https://wiki.onap.org/display/DW/Controllers

## Introduction
We classify ONAP Application Controller into the Network Services domain as ONAP Application Controller provides the capability to orchestrate and control network services, Virtual Network Functions. We also classify it into the Cloud domain because it orchestrates and manages resources within cloud environments.

## Why Integrate
The ONAP APPC adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ONAP Application Controller. With this adapter you have the ability to perform operations such as:

- Add and Remove Inventory Devices: Through IAP, a new device can be added to the network inventory so that it can be managed by the Itential Automation Gateway (IAG). A device that is no longer utilized from the network inventory can also be removed through IAP.

- Perform Pre and Post Checks: IAP allows for the ability to perform pre and post checks of a device configuration when making modifications to the device.
- Run Commands on a Device: Run individual commands on a device to help determine state, troubleshoot, and set up temporary conditions.

- Add and Remove Device Component to Monitoring: When a new device is turned up, it can be automatically added to monitoring. When a device is turned down, it can be automatically removed from monitoring.

- Manage Device or Device Components during Maintenance: When changes are being made on a device, polling can be disabled to avoid false data. During this time, alerts being sent to Operations can also be paused.

- Group Management: Add or remove devices or device components from groups.

- SLA Report Generation: An alert can be set up in ONAP APPC if there is an issue with an IAP automation.

- Capacity Planning: When a workflow fix is underway, an alert can be set to ignore so that Operation teams do not have to be notified of extraneous information. Automatically clear alerts after a workflow has been completed to address an issue.

## Additional Product Documentation
The [API documents for ONAP APPC](https://wiki.onap.org/display/DW/APPC+API)

