## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for ONAP Application Controller. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for ONAP Application Controller.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the ONAP Application Controller. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">healthCheckAppc(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:health-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configModify(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:config-modify?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configure(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:configure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configScaleOut(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:config-scale-out?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionStatus(body, callback)</td>
    <td style="padding:15px">The ActionStatus command returns that state of any action request that has been previously submitted to an APPC instance for a specified VNF. This enables the client to know the status of a previous request and helps them decide if they should reissue a request.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:action-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attachVolume(body, callback)</td>
    <td style="padding:15px">The AttachVolume command attaches a cinder volume to a VM via an Openstack command. Cinder is a Block Storage service for OpenStack. It’s designed to present storage resources to end users that can be consumed by the OpenStack Compute Project (Nova). The short description of Cinder is that it virtualizes the management of block storage devices and provides end users with a self service API to request and consume those resources without requiring any knowledge of where their storage is actually d...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:attach-volume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">audit(body, callback)</td>
    <td style="padding:15px">The Audit command compares the configuration of the VNF associated with the current request against the most recent configuration that is stored in APPC’s configuration database. A successful Audit means that the current VNF configuration matches the latest APPC stored configuration. A failed Audit indicates that the configurations do not match. This command can be applied to any VNF type. The only restriction is that the VNF has been onboarded in self-service mode (which requires that the VNF s...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:audit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkLock(body, callback)</td>
    <td style="padding:15px">The CheckLock command returns true if the specified VNF is locked; otherwise, false is returned. A CheckLock command is deemed successful if the processing completes without error, whether the VNF is locked or not. The command returns only a single response with a final status. Note that APPC locks the target VNF during any VNF command processing, so a VNF can have a locked status even if no Lock command has been explicitly called. The CheckLock command returns a specific response structure that...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:checklock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configBackup(body, callback)</td>
    <td style="padding:15px">Stores the current VNF configuration on a local file system (not in APPC). This is limited to Ansible and Chef. There can only be one stored configuration (if there is a previously saved configuration, it is replaced with the current VNF configuration). A successful ConfigBackup request returns a success response. A failed ConfigBackup action returns a failure response code and the specific failure message in the response block.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:config-backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">configRestore(body, callback)</td>
    <td style="padding:15px">Applies a previously saved configuration to the active VNF configuration. This is limited to Ansible and Chef. There can only be one stored configuration. A successful ConfigRestore request returns a success response. A failed ConfigRestore action returns a failure response code and the specific failure message in the response block.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:config-restore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detachVolume(body, callback)</td>
    <td style="padding:15px">The DetachVolume command detaches a cinder volume from a VM via an Openstack command. Cinder is a Block Storage service for OpenStack. It’s designed to present storage resources to end users that can be consumed by the OpenStack Compute Project (Nova). The short description of Cinder is that it virtualizes the management of block storage devices and provides end users with a self-service API to request and consume those resources without requiring any knowledge of where their storage is actually...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:detach-volume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">evacuate(body, callback)</td>
    <td style="padding:15px">Evacuates a specified VM from its current host to another. After a successful evacuate, a rebuild VM is performed if a snapshot is available (and the VM boots from a snapshot). The host on which the VM resides needs to be down. If the target host is not specified in the request, it will be selected by relying on internal rules to evacuate. The Evacuate action will fail if the specified target host is not UP/ENABLED. After Evacuate, the rebuild VM can be disabled by setting the optional rebuild-v...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:evacuate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lock(body, callback)</td>
    <td style="padding:15px">Use the Lock command to ensure exclusive access during a series of critical LCM commands. The Lock action will return a successful result if the VNF is not already locked or if it was locked with the same request-id, otherwise the action returns a response with a reject status code. Lock is a command intended for APPC and does not execute an actual VNF command. Instead, lock will ensure that ONAP is granted exclusive access to the VNF. When a VNF is locked, any subsequent sequential commands wit...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:lock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">migrate(body, callback)</td>
    <td style="padding:15px">Migrates a running target VM from its current host to another. A destination node will be selected by relying on internal rules to migrate. Migrate calls a command in order to perform the operation. Migrate suspends the guest virtual machine, and moves an image of the guest virtual machine’s disk to the destination host physical machine. The guest virtual machine is then resumed on the destination host physical machine and the disk storage that it used on the source host physical machine is free...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:migrate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">quiesceTraffic(body, callback)</td>
    <td style="padding:15px">The QuiesceTraffic LCM action gracefully stops the traffic on the VNF (i.e., no service interruption for traffic in progress). All application processes are assumed to be running but no traffic is being processed. This command is executed using an Ansible playbook or Chef cookbook.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:quiesce-traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reboot(body, callback)</td>
    <td style="padding:15px">The Reboot is used to reboot a VM. There are two types supported: HARD and SOFT. A SOFT reboot attempts a graceful shutdown and restart of the server. A HARD reboot attempts a forced shutdown and restart of the server. The HARD reboot corresponds to the power cycles of the server. NOTE: The command implementation is based on OpenStack functionality. For further details, see http://developer.openstack.org/api-ref/compute/.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:reboot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebuild(body, callback)</td>
    <td style="padding:15px">Recreates a target VM instance to a known, stable state. Rebuild calls an OpenStack command immediately and therefore does not expect any prerequisite operations to be performed, such as shutting off a VM. Rebuild VM uses the snapshot provided by the snapshot-id (if provided). If not provided, the latest snapshot is used. If there are no snapshots, it uses the (original) Glance image. APPC rejects a rebuild request if it determines the VM boots from a Cinder Volume NOTE: The command implementati...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:rebuild?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restart(body, callback)</td>
    <td style="padding:15px">Use the Restart command to restart a VM.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resumeTraffic(body, callback)</td>
    <td style="padding:15px">The ResumeTraffic LCM action resumes processing traffic on a VNF that has been previously quiesced. This command is executed using an Ansible playbook or Chef cookbook. Request Structure: The payload does not have any parameters.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:resume-traffic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">snapshot(body, callback)</td>
    <td style="padding:15px">Creates a snapshot of a VM. The Snapshot command returns a customized response containing a reference to the newly created snapshot instance if the action is successful. This command can be applied to a VM in any VNF type. The only restriction is that the particular VNF should be built based on the generic heat stack. Note: Snapshot is not reliable unless the VM is in a stopped, paused, or quiesced (no traffic being processed) status. It is up to the caller to ensure that the VM is in one of the...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:snapshot?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">start(body, callback)</td>
    <td style="padding:15px">Use the Start command to start a VM that is stopped. NOTE: The command implementation is based on Openstack functionality. For further details, see http://developer.openstack.org/api-ref/compute/.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startApplication(body, callback)</td>
    <td style="padding:15px">Starts the VNF application, if needed, after a VM is instantiated/configured or after VM start or restart. Supported using Chef cookbook or Ansible playbook only. A successful StartApplication request returns a success response. A failed StartApplication action returns a failure response code and the specific failure message in the response block.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:start-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stop(body, callback)</td>
    <td style="padding:15px">Use the Stop command to stop a VM that was running.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopApplication(body, callback)</td>
    <td style="padding:15px">Stops the VNF application gracefully (not lost traffic), if needed, prior to a Stop command. Supported using Chef cookbook or Ansible playbook only. A successful StopApplication request returns a success response. A failed StopApplication action returns a failure response code and the specific failure message in the response block.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:stop-application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sync(body, callback)</td>
    <td style="padding:15px">The Sync action updates the current configuration in the APPC store with the running configuration from the device. A successful Sync returns a success status. A failed Sync returns a failure response status and failure messages in the response payload block. This command can be applied to any VNF type. The only restriction is that the VNF has been onboarded in self-service mode (which requires that the VNF supports a request to return the running configuration).</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlock(body, callback)</td>
    <td style="padding:15px">Run the Unlock command to release the lock on a VNF and allow other clients to perform LCM commands on that VNF. Unlock is a command intended for APPC and does not execute an actual VNF command. Instead, unlock will release the VNF from the exclusive access held by the specific request-id allowing other requests for the VNF to be accepted. The Unlock command will result in success if the VNF successfully unlocked or if it was already unlocked, otherwise commands will be rejected. The Unlock comm...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:unlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeBackout(body, callback)</td>
    <td style="padding:15px">The Sync action updates the current configuration in the APPC store with the running configuration from the device. A successful Sync returns a success status. A failed Sync returns a failure response status and failure messages in the response payload block. This command can be applied to any VNF type. The only restriction is that the VNF has been onboarded in self-service mode (which requires that the VNF supports a request to return the running configuration).</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:upgrade-backout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeBackup(body, callback)</td>
    <td style="padding:15px">The UpgradeBackup LCM action does a full backup of the VNF data prior to an upgrade. The backup is done on the Ansible or Chef server in a location that is specified in the playbook or cookbook. If there is an existing backup, it is overwritten by the new backup. This command is executed using an Ansible playbook or Chef cookbook. Request Structure: The payload does not have any parameters required.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:upgrade-backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradePostCheck(body, callback)</td>
    <td style="padding:15px">Success: If the UpgradePostCheck run successfully, it returns a success status code 400. The response payload contains the results of the check (Completed or Failed).</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:upgrade-post-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradePreCheck(body, callback)</td>
    <td style="padding:15px">The UpgradePreCheck LCM action checks that the VNF has the correct software version needed for a software upgrade. This command can be executed on a running VNF (i.e. processing traffic). This command is executed using an Ansible playbook or Chef cookbook.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:upgrade-pre-check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeSoftware(body, callback)</td>
    <td style="padding:15px">The UpgradePreCheck LCM action checks that the VNF has the correct software version needed for a software upgrade. This command can be executed on a running VNF (i.e. processing traffic). This command is executed using an Ansible playbook or Chef cookbook.</td>
    <td style="padding:15px">{base_path}/{version}/restconf/operations/appc-provider-lcm:upgrade-software?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
